import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {addPost,updatePostText,updateMessageText,sendMessage} from "./Redux/state";

export let rerenderEntireTree = (state) =>
{
ReactDOM.render(
  <React.StrictMode>
    <App state={state} addPost={addPost} updatePostText={updatePostText} updateMessageText={updateMessageText} sendMessage={sendMessage}/>
  </React.StrictMode>,
  document.getElementById('root')
)
};

