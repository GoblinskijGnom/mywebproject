import d from "../Dialogs.module.css"
import di from "./DialogItem.module.css"
import { NavLink } from "react-router-dom"

const DialogItem = (props) => {
    let path = "/dialogs/" + props.id;
    return (
        <div className={`${d.dialog} ${di.dialog}`}>
            <NavLink to={path} activeClassName={d.active}><img src={props.state[props.id-1].image}/>{props.name}</NavLink>
        </div>
    );
}

export default DialogItem;