import d from "./Dialogs.module.css"
import DialogItem from "./DialogItem/DialogItem"
import Message from "./Message/Message"
import React from "react"

const Dialogs = (props) => {

    let DialogsElements = props.state.DialogsData.map(d => <DialogItem state={props.state.DialogsData} name={d.name} id={d.id} />);

    let MessagesElements = props.state.MessagesData.map(m => <Message className={d.message} message={m.message} />);

    let sendMessageRef = React.createRef();

    let addMessageButton = () => 
    {
        props.sendMessage(props.messageText);
    }

    let onChangeMessage = () =>
    {
        let messageText = sendMessageRef.current.value
        props.updateMessageText(messageText);
    }
    return (
        <div className={d.dialogs}>
            <div className={d.dialogsItems}>
                {DialogsElements}
            </div>
            <div>
                <div className={d.messages}>
                {MessagesElements}
            </div>
            <div className={d.addMessage}>
                <textarea ref={sendMessageRef} value={props.state.messageText} onChange={onChangeMessage}/>
                <button onClick={addMessageButton}>Send</button>
            </div>
            </div>
            
        </div>
    );
}

export default Dialogs;