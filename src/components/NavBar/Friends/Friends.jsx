import { findRenderedDOMComponentWithClass } from "react-dom/cjs/react-dom-test-utils.development";
import f from "./Friends.module.css";

const Friends = (props) =>
{
    return(
        <div className={f.Friends}>
            <div className={f.Head}>
                Friends
            </div>
            <div className={f.image}>
                <img src={props.state[0].image}/>
                <img src={props.state[1].image}/>
                <img src={props.state[2].image}/>
            </div>
           

        </div>
    );
}

export default Friends;