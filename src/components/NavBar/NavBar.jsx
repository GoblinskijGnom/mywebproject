import { NavLink } from "react-router-dom";
import Friends from "./Friends/Friends";
import n from "./NavBar.module.css"

const NavBar = (props) => {
  return (
    <div className={n.nav}>

      <div className={n.item}>
        <NavLink to="/profile" activeClassName={n.activeLink}> Profile</NavLink>
      </div>
      <div className={n.item}>
        <NavLink to="/dialogs" activeClassName={n.activeLink}>Dialogs</NavLink>
      </div>
      <div className={n.item}>
        <NavLink to="/news" activeClassName={n.activeLink}>News</NavLink>
      </div>
      <div className={n.item}>
        <Friends state={props.state.FriendsData}/>
      </div>
      {/* <div className={n.item}>
               <a>Music</a>
             </div>
              <div className={n.item}>
               <a>Settings</a>
              </div> */}

    </div>
  );
}

export default NavBar;