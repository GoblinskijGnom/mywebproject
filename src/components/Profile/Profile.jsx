import MyPosts from "./MyPosts/MyPosts.jsx";
import p from "./Profile.module.css"
import Head from "./Head/Head";
import Avatar from "./Avatar/Avatar.jsx";

const Profile = (props) => {
  return (
    <div className={p.Profile}>
      <Head className={p.Head} />
      <div className={p.AvaDescr}>
        <Avatar /> 
        <div className={p.Description}>
          description
        </div> 
        </div>
      <div>
        <MyPosts state={props.state.PostsData} addPost={props.addPost} postText={props.state.postText} updatePostText={props.updatePostText}/>
      </div>
    </div>

  );
}

export default Profile;