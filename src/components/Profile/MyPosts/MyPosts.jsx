import p from "./MyPosts.module.css"
import Post from "./Post/Post"
import React from "react"



const MyPosts = (props) => {

  let PostsElements = props.state.map(p => <Post message={p.message} likesCount={p.likesCount} />);
  let AddPostRef = React.createRef();
  let AddPostButton = () => 
  {
     props.addPost(postMessage);
  }

  let onChangePost = () =>
  {
    let postMessage = AddPostRef.current.value
    props.updatePostText(postMessage);
  }


  return (
    <div className={p.MyPosts}>
      <div>
        My posts
        </div>
      <div className={p.AddPost}>
        <textarea ref={AddPostRef} onChange={onChangePost} value={props.postText}/>
        <button onClick={AddPostButton}>Add Post</button>
      </div>
      <div className={p.posts}>
        {PostsElements}
      </div>
    </div>
  );
}




export default MyPosts;