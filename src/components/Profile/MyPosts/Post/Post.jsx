import p from "./Post.module.css"

const Post = (props) => {
  return (

    <div className={p.item} >
      <div>
        <img src="https://www.kinonews.ru/insimgs/2019/newsimg/newsimg87089.jpg"/> {props.message}
      <div />
      <div className={p.actions}>
        <span>like</span> {props.likesCount}
        </div>
    </div>
    </div>


  );
}

export default Post;