import logo from './logo.svg';
import { BrowserRouter, Route } from "react-router-dom";
import './App.css';
import Header from './components/Header/Header';
import NavBar from './components/NavBar/NavBar';
import Profile from './components/Profile/Profile';
import Dialogs from "./components/Dialogs/Dialogs";
import News from "./components/News/News";


const App = (props) => {
  return (
    <BrowserRouter>
      <div className='app-wrapper'>
        <Header />
        <NavBar state={props.state.NavBar}/>
        <div className="app-wrapper-content">
          <Route path="/dialogs" render={() => <Dialogs state={props.state.DialogPage}   
                                                        sendMessage={props.sendMessage} 
                                                        updateMessageText={props.updateMessageText} />} />
         
          <Route path="/profile" render={() => <Profile state={props.state.ProfilePage}
                                                              addPost={props.addPost}
                                                              updatePostText={props.updatePostText}/>} />
          <Route path="/news" component={News} />
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
